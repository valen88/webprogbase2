let express = require('express');
let router = express.Router();

router.use(express.json());
router.use(express.static('public'));
router.use(express.static('views'));
router.use(express.static('models'));
router.use(express.static('data'));
////////////////////////////////////////////////////////////////////////////////////


router.get('/about', function (req, res) {
   res.render("about", { title: "About page", user: req.user });
});

router.get('/',function (req, res) {
   console.log(req.user);
   res.render('index', { title: "Main page", user: req.user });
});

module.exports = router;