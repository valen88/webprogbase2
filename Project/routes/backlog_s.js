const backlog = require('../models/backlog.js');
const story = require('../models/story.js');

const mongoose = require('mongoose');

let express = require('express');
let router = express.Router();

router.use(express.json());
router.use(express.static('public'));
router.use(express.static('views'));
router.use(express.static('models'));
router.use(express.static('data'));

//////
const config = require("../config.js");
const cloudinaryStorage = require('multer-storage-cloudinary');
const multer = require('multer');
const cloudinary = require('cloudinary');
cloudinary.config({
   cloud_name: config.cloudinary_cloud_name,
   api_key: config.cloudinary_api_key,
   api_secret: config.cloudinary_api_secret
});
const storage = cloudinaryStorage({
   cloudinary: cloudinary,
   folder: 'folder-name',
   allowedFormats: ['jpg', 'png'],
   transformation: [{ width: 400, height: 400, crop: "limit" }]
});
const upload = multer({ storage: storage });

////////////////////////////////////////////////////////////////////////////////////

router.get('/backlogs', checkAuth, function (req, res) {
   const pageSize = 4;
   if(req.query.search){
      let search = req.query.search.toLowerCase();
      backlog.findByName(search,req.user.groupName)
      .then(filteredStories =>{
         let searchResult;
         for (let i = 0; i < filteredStories.length; i++) {
            if (!filteredStories[i].name.toLowerCase().includes(search)) {
               filteredStories.splice(i, 1);
               i--;
            }
         }
         searchResult = (filteredStories.length === 0) ? 'No results for ' + search: 'Displaying backlogs for:' + search + ".   Results found:" +filteredStories.length ;

         let pagesTotal = Math.ceil(filteredStories.length / pageSize);
         let page = (typeof req.query.p === 'undefined' || req.query.p < 1 || req.query.p > pagesTotal) ? 1 : parseInt(req.query.p);
         let prev = (page !== 1) ? (page - 1) : 1;
         let next = (page !== pagesTotal) ? (page + 1) : pagesTotal;
         let index = (page - 1) * pageSize;

         res.render('backlogs', {
            searchResult: searchResult,
            title: "Backlogs",
            backlogs: filteredStories.slice((page - 1) * pageSize, index + pageSize),
            page: page,
            prev : "?p=" + prev,
            next : "?p=" + next,
            pageCount: pagesTotal,
            user: req.user,
            filteredSearch: "&search=" + String(req.query.search),

         });
      })
   }
   else {
      console.log(req.user.groupName);
      backlog.getAll(req.user.groupName)
         .then(backlogs => {
            let pagesTotal = Math.ceil(backlogs.length / pageSize);
            let page = (typeof req.query.p === 'undefined' || req.query.p < 1 || req.query.p > pagesTotal) ? 1 : parseInt(req.query.p);
            let prev = (page !== 1) ? (page - 1) : 1;
            let next = (page !== pagesTotal) ? (page + 1) : pagesTotal;
            let index = (page - 1) * pageSize;
            res.render('backlogs', {
               backlogs: backlogs.slice((page - 1) * pageSize, index + pageSize),
               page: page,
               prev: prev,
               next: next,
               pageCount: pagesTotal,
               user: req.user
            });
         })
         .catch(err => res.status(500).send(err.toString()));
   }
});

router.get('/delete', checkAuth, function (req, res) {
   let storyId = req.query.storyid;
   let bid = req.query.backlogid;
   let whom = storyId;
   let how = { backlogID: "" };
   story.insertBL(whom, how)
      .then(() => res.redirect('/backlogs/' + bid))
      .catch(err => res.status(500).send(err.toString()));
});

router.route('/new_backlog')
   .get(checkAuth, upload.single('file'), function (req, res) {
      res.render('new_backlog', { title: "new_backlog", user: req.user });
   })
   .post(upload.single('file'), function (req, res) {
      if (req.file) {
         let bl = new backlog(req.body.name, req.file.url, req.user.groupName);
         backlog.insert(bl)
            .then(newBacklog => {
               res.redirect('backlogs/' + newBacklog.id);
            })
            .catch(res.status(500));
      } else {
         let bl = new backlog(req.body.name, "https://res.cloudinary.com/drpv1113r/image/upload/v1573195885/onmtedklywezi7enlxmn.png", req.user.groupName);
         console.log("IN  BACKLOOOOOOOOOG" + req.user.groupName);
         backlog.insert(bl)
            .then(function (newBacklog) {
               res.redirect('backlogs/' + newBacklog.id);
            })
            .catch(res.status(500));
      }
   });

router.route('/backlogs/:id')
   .get(function (req, res) {
      if (!mongoose.Types.ObjectId.isValid(req.params.id)) return res.status(404).render('not_found');

      let tBacklog;
      backlog.getById(req.params.id)
         .then((_backlog) => {
            tBacklog = _backlog;
            return story.getAllStoriesByBacklogId(_backlog.id);
         })
         .then((storen) => {
            let promises = [];
            promises.push(storen);
            let result = story.getAllFree();
            promises.push(result);
            return Promise.all(promises);
         })
         .then(arrayOfpROMISES => {
            res.render('backlog', {
               user_stories: arrayOfpROMISES[0],
               name: tBacklog.name,
               avaUrl: tBacklog.avaUrl,
               user_stories2: arrayOfpROMISES[1],
               user: req.user,
               bid: req.params.id
            });
         }).catch(err => console.error(err));
   })
   .post(upload.none(), function (req, res) {

      if (req.body.delete) {
         backlog.deleteById(req.params.id)
            .then(() => res.redirect('/backlogs'))
            .catch(err => res.status(500).send(err.toString()));
      } if (req.body.submit) {
         let storyIDtoChange = req.body.stories;
         const whom = storyIDtoChange;
         const how = { backlogID: req.params.id };
         story.insertBL(whom, how)
            .then(() => res.redirect('/backlogs/' + req.params.id))
            .catch(err => res.status(500).send(err.toString()));
      }
   });
////////////////////////////////////////////////////////////////////////////////////


function checkAuth(req, res, next) {
   if (!req.user) return res.sendStatus(401); // 'Not authorized'
   next();  // пропускати далі тільки аутентифікованих
}

module.exports = router;