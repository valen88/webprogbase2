let express = require('express');
let developer = express.Router();
developer.use(express.static('../public'));
developer.use(express.static('../views'));
developer.use(express.static('../models'));
developer.use(express.static('../data'));
////////////////////////////////////////////////////////////////////////////////////

developer.get('/',function (req, res) {
    res.render('developerv1.mst');
});
////////////////////////////////////////////////////////////////////////////////////

module.exports = developer;

