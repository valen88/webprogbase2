const user = require('../models/user.js');
const mongoose = require('mongoose');

let express = require('express');
let router = express.Router();

router.use(express.json());
router.use(express.static('public'));
router.use(express.static('views'));
router.use(express.static('models'));
router.use(express.static('data'));

//////
const config = require("../config.js");
const cloudinaryStorage = require('multer-storage-cloudinary');
const multer = require('multer');
const cloudinary = require('cloudinary');
cloudinary.config({
   cloud_name: config.cloudinary_cloud_name,
   api_key: config.cloudinary_api_key,
   api_secret: config.cloudinary_api_secret
});
const storage = cloudinaryStorage({
   cloudinary: cloudinary,
   folder: 'folder-name',
   allowedFormats: ['jpg', 'png'],
   transformation: [{ width: 400, height: 400, crop: "limit" }]
});
const upload = multer({ storage: storage });
////////////////////////////////////////////////////////////////////////////////////
router.get('/users', checkAuth,checkAdmin, function (req, res) {
   const pageSize = 4;
   user.getAll()
      .then(allUsers => {
         let pagesTotal = Math.ceil(allUsers.length / pageSize);
         let page = (typeof req.query.p === 'undefined' || req.query.p < 1 || req.query.p > pagesTotal) ? 1 : parseInt(req.query.p);
         let prev = (page !== 1) ? (page - 1) : 1;
         let next = (page !== pagesTotal) ? (page + 1) : pagesTotal;
         res.render('users', {
            title: "Users",
            users: allUsers.slice(page * pageSize - pageSize, page * pageSize),
            user: req.user,
            page: page,
            prevPage: "?p=" + prev,
            nextPage: "?p=" + next,
            pageCount: pagesTotal,

         });
      })
      .catch(error => alert(error.message));
});

router.route('/update_user/:id')
   .get(checkAuth, function (req, res) {
      if (!mongoose.Types.ObjectId.isValid(req.params.id)) return res.status(404).render('not_found');
      user.getById(req.params.id)
         .then(useren => {
            res.render('update_user', {
               title: useren.login,
               login: useren.login,
               fullname: useren.fullname,
               avaUrl: useren.avaUrl,
               about: useren.about,
               user: useren,
            });
         })
         .catch(err => res.status(500).send(err.toString()));
   })
   .post(checkAuth, upload.single('file'), function (req, res) {
      let newAva;

      user.getById(req.params.id)
         .then(useren => {
            newAva = useren.avaUrl;
            if (req.file) { newAva = req.file.url; }
            let dataToUpdate = {
               login: req.body.login,
               fullname: req.body.fullname,
               about: req.body.about,
               groupName: req.body.groupName,
               avaUrl: newAva
            };
            console.log(newAva);
            return user.update(req.params.id, dataToUpdate);
         })
         .then(() => {
            res.redirect('/user/' + req.params.id);
         })
         .catch(err => res.status(500).send(err.toString()));
   });

router.route('/user/:id')
   .get(checkAuth, function (req, res) {
      if (!mongoose.Types.ObjectId.isValid(req.params.id)) return res.status(404).render('not_found');

      let changable = "ADMIN";
      user.getById(req.params.id)
         .then(useren => {
            if (!useren) {
               return res.status(404).render('not_found');
            } else {
               if (useren.role) {
                  changable = "USER";
               }
               res.render('user', {
                  title: useren.login,
                  login: useren.login,
                  fullname: useren.fullname,
                  registeredAt: useren.registeredAt,
                  avaUrl: useren.avaUrl,
                  about: useren.about,
                  user: req.user,
                  role: req.user.role,
                  changable: changable,
                  groupName: req.user.groupName
               });
            }
         })
         .catch(err => res.status(500).send(err.toString()));
   })
   .post(checkAuth, upload.none(), function (req, res) {
      let ide = req.params.id;
      if (req.body.changeRole) {
         user.getById(ide)
            .then(_user => {
               return user.changeRole(_user);
            })
            .then(() => res.redirect('/user/' + req.params.id))
            .catch(err => res.status(500).send(err.toString()));
      }
      if (req.body.updateUser) {
         res.redirect('/update_user/' + req.params.id);
      }
      if (req.body.deleteUser) {
         if (req.user.role) {
            user.deleteById(ide)
               .then(() => res.redirect('/users'))
               .catch(err => res.status(500).send(err.toString()));
         } else {
            user.deleteById(ide)
               .then(() => req.logout())
               .then(() => res.redirect('/auth/login'))
               .catch(err => res.status(500).send(err.toString()));
         }
      }

   });
////////////////////////////////////////////////////////////////////////////////////
function checkAuth(req, res, next) {
   if (!req.user) return res.sendStatus(401); // 'Not authorized'
   next();  // пропускати далі тільки аутентифікованих
}
function checkAdmin(req, res, next) {
   if (!req.user) res.sendStatus(401); // 'Not authorized'
   else if (!req.user.role) res.sendStatus(403); // 'Forbidden'
   else next();  // пропускати далі тільки аутентифікованих із роллю 'admin'
}



module.exports = router;