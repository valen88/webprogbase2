const express = require('express');
const user = require('../models/user.js');
const api = express.Router();
const config = require("../config.js");
const crypto = require('crypto');
const BasicStrategy = require('passport-http').BasicStrategy;
const backlog = require('../models/backlog.js');
const story = require('../models/story.js');
const cloudinary = require('cloudinary');
const cloudinaryStorage = require('multer-storage-cloudinary');
const mongoose = require('mongoose');

const multer = require('multer');
const storage = cloudinaryStorage({
    cloudinary: cloudinary,
    folder: 'folder-name',
    allowedFormats: ['jpg', 'png'],
    transformation: [{ width: 400, height: 400, crop: "limit" }]
});
cloudinary.config({
    cloud_name: config.cloudinary_cloud_name,
    api_key: config.cloudinary_api_key,
    api_secret: config.cloudinary_api_secret
});
const serverSalt = config.secretKey;
function sha512(password, salt) {
    const hash = crypto.createHmac('sha512', salt);
    hash.update(password);
    const value = hash.digest('hex');
    return {
        salt: salt,
        passwordHash: value
    };
};
const upload = multer({ storage: storage });

const passport = require('passport');

passport.use(new BasicStrategy(
    function (username, password, done) {
        let hash = sha512(password, serverSalt).passwordHash;
        user.getUserByLoginAndPasshash(username, hash)
            .then(user => {
                done(user ? null : 'No user', user);
            });
    }
));
api.get('/', function (req, res) {
    res.json({});
});
api.get('/me', checkAuth,passport.authenticate('basic', { session: false }), function (req, res) {
    let user = req.user;
    res.send(user);
});
///////////////
api.get('/backlogs', checkAuth,passport.authenticate('basic', { session: false }),  function (req, res) {
    const pageSize = 3;
    backlog.getAll(req.user.groupName)
        .then(backlogs => {
            let pagesTotal = Math.ceil(backlogs.length / pageSize);
            let page = (typeof req.query.p === 'undefined' || req.query.p < 1 || req.query.p > pagesTotal) ? 1 : parseInt(req.query.p);
            let index = (page - 1) * pageSize;
            backlogs = backlogs.slice((page - 1) * pageSize, index + pageSize),
                res.json(backlogs);
        })
        .catch(err => res.status(500).send(err.toString()));
});
api.get('/backlogs/:id',checkAuth, upload.none(), function (req, res) {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) return res.status(404).send({ error: 'Bad Request' });
    backlog.getById(req.params.id)
        .then((_backlog) => {
            return story.getAllStoriesByBacklogId(_backlog.id);
        })
        .then((getAllStoriesByBacklogId) => {
            let promises = [];
            promises.push(getAllStoriesByBacklogId);
            let result = story.getAllFree();
            promises.push(result);
            return Promise.all(promises);
        })
        .then(arrayOfpROMISES => {
            let asdas = arrayOfpROMISES[0];///////////getAllStoriesByBacklogId
            asdas.push({});
            asdas.push(arrayOfpROMISES[1][0]);////////////////getAllFree
            res.json(asdas);
        })
        .catch(res.status(500));
});
api.post('/backlogs/:id', checkAuth,passport.authenticate('basic', { session: false }), upload.none(), function (req, res) {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) return res.status(404).send({ error: 'Bad Request' });

    if (req.body.delete) {
        backlog.deleteById(req.params.id)
        .then(() => res.status(202).res.json({}))
        .catch(err => res.status(500).send(err.toString()));
    } if (req.body.submit) {
        let storyIDtoChange = req.body.stories;
        const whom = storyIDtoChange;
        const how = { backlogID: req.params.id };
        story.insertBL(whom, how)
            .then(() => res.status(202).res.json({}))
            .catch(err => res.status(500).send(err.toString()));
    }
    if (req.body.update) {
        backlog.update(req.params.id, req.body.name)
        .then(() => res.status(202).res.json({}))
        .catch(err => res.status(500).send(err.toString()));
    }
});
api.post('/new_backlog', checkAuth,function (req, res) {
    upload.single('file')(req, res, err => {
        if (err) {
            res.status(404).send({ error: 'Bad Request' });
        }
        else {
            if (req.file) {
                backlog.insert(new backlog(req.body.name, req.file.url))
                    .then(() => {
                        res.status(202).res.json({});
                    })
                    .catch(res.status(500));
            } else {
                backlog.insert(new backlog(req.body.name, ""))
                    .then(()=> {
                        res.status(202).res.json({});
                    })
                    .catch(res.status(500));
            }
        }
    });
});
///////////////
api.get('/user_stories', checkAuth,passport.authenticate('basic', { session: false }), function (req, res) {
    const pageSize = 3;
    if (req.query.search) {
        let search = req.query.search.toLowerCase();
        story.findByRole(search)
            .then((filteredStories) => {
                for (let i = 0; i < filteredStories.length; i++) {
                    if (!filteredStories[i].role.toLowerCase().includes(search)) {
                        filteredStories.splice(i, 1);
                        i--;
                    }
                }
                let pagesTotal = Math.ceil(filteredStories.length / pageSize);
                let page = (typeof req.query.p === 'undefined' || req.query.p < 1 || req.query.p > pagesTotal) ? 1 : parseInt(req.query.p);
                let user_stories = filteredStories.slice(page * pageSize - pageSize, page * pageSize);
                res.status(200).json(user_stories);
            })
            .catch(err => res.send(err));
    }
    else {
        story.getAll()
            .then(stories => {
                let pagesTotal = Math.ceil(stories.length / pageSize);
                let page = (typeof req.query.p === 'undefined' || req.query.p < 1 || req.query.p > pagesTotal) ? 1 : parseInt(req.query.p);
                let user_stories = stories.slice(page * pageSize - pageSize, page * pageSize);
                res.status(200).json(user_stories);
            })
            .catch(err => res.send(err));
    }
});
api.get('/user_stories/:id', checkAuth,function (req, res) {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) return res.status(400).send({ error: 'Bad Request' });
    story.getById(req.params.id)
        .then(story =>
            res.status(200).json(story))
        .catch(err => res.status(500).send(err.toString()));
});
api.post('/user_stories/:id', checkAuth,passport.authenticate('basic', { session: false }),  function (req, res) {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) return res.status(400).send({ error: 'Bad Request' });
    story.deleteById(req.params.id)
        .then(() => res.status(200).json({}))
        .catch(err => res.status(500).send(err.toString()));
});
api.post("/new_story",checkAuth, passport.authenticate('basic', { session: false }),upload.single('file'), function (req, res) {
    if (req.file) {
        story.insert(new story(null, Date.now(), req.body.role, req.body.iWantTo, req.body.points, req.file.url))
            .then(() => {
                res.status(201).json({});
            })
            .catch(res.status(500));
    } else {
        if ((Number.isInteger(req.body.points)) || (typeof req.body.role) !== 'string' || (typeof req.body.iWantTo) !== 'string') { return res.status(400).send({ error: 'Bad Request' }); }
        else {
            story.insert(new story(null, Date.now(), req.body.role, req.body.iWantTo, req.body.points, ""))
                .then(() => {
                    res.status(201).json({});
                })
                .catch(res.status(500));
        }
    }
});
///////////////
api.get('/users', passport.authenticate('basic', { session: false }), checkAdmin, function (req, res) {
    const pageSize = 3;

    user.getAll()
        .then(allUsers => {
            let pagesTotal = Math.ceil(allUsers.length / pageSize);

            let page = (typeof req.query.p === 'undefined' || req.query.p < 1 || req.query.p > pagesTotal) ? 1 : parseInt(req.query.p);
            let index = (page - 1) * pageSize;
            let all = allUsers.slice((page - 1) * pageSize, index + pageSize);
            res.json(all);
        })
        .catch(err => res.status(500).send(err.toString()));
});
api.get('/user/:id', checkAuth,passport.authenticate('basic', { session: false }), function (req, res) {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) return res.status(400).send({ error: 'Bad Request' });
    {
      let changable = "ADMIN";
      user.getById(req.params.id)
         .then(useren => {
            if (useren.role) {
               changable = "USER";
            }
            res.status(200).json(useren);
         })
         .catch(err => res.status(500).send(err.toString()));
   }
});
api.post('/user/:id', passport.authenticate('basic', { session: false }), upload.none(), checkAdmin, function (req, res) {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) return res.status(400).send({ error: 'Bad Request' });
    let ide = req.params.id;
    if (req.body.changeRole) {
       user.getById(ide)
          .then(_user => {
             return user.changeRole(_user);
          })
          .then(() => res.status(200).json({}))
          .catch(err => res.status(500).send(err.toString()));
    }
    if (req.body.updateUser) {
       res.status(200).json({});
    }
    if (req.body.deleteUser) {
       if (req.user.role) {
          user.deleteById(ide)
          .then(() => res.status(200).json({}))
             .catch(err => res.status(500).send(err.toString()));
       } else {
          user.deleteById(ide)
             .then(() => req.logout())
             .then(() => res.status(200).json({}))
             .catch(err => res.status(500).send(err.toString()));
       }
    }
 });
api.post('/change_user/:id',checkAuth, passport.authenticate('basic', { session: false }), upload.none(), function (req, res) {
    let newAva;
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) return res.status(400).send({ error: 'Bad Request' });
    user.getById(req.params.id)
       .then(useren => {
          newAva = useren.avaUrl;
          if (req.file) { newAva = req.file.url; }
          let dataToUpdate = {
             login: req.body.login,
             fullname: req.body.fullname,
             about: req.body.about,
             avaUrl: newAva
          };
          return user.update(req.params.id, dataToUpdate);
       })
       .then(() => res.status(200).json({}))
       .catch(err => res.status(500).send(err.toString()));
 });
///////////////
api.get('/validlogin/:login', (req, res) => {
    user.checkLoginUniqueness(req.params.login)
        .then((user) => {
            if (!user) {
                res.status(200).send({ message: 'Valid Username' });
            }
            else {
                res.status(409).send({
                    error: 'Non Unique Username',
                    username: user.login
                });
            }
        })
        .catch(err => {
            res.status(500).send({ error: 'Internal Server Error' });
        });
});
function checkAuth(req, res, next) {
    if (!req.user) return res.sendStatus(401); // 'Not authorized'
    next();  // пропускати далі тільки аутентифікованих
}
function checkAdmin(req, res, next) {
    if (!req.user) res.sendStatus(401); // 'Not authorized'
    else if (!req.user.role) res.sendStatus(403); // 'Forbidden'
    else next();  // пропускати далі тільки аутентифікованих із роллю 'admin'
}
module.exports = api;