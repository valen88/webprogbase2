
const user = require('../models/user.js');
//const mongoose = require('mongoose');
const passport = require('passport');
const express = require('express');
const router = express.Router();
const BitbucketStrategy = require('passport-bitbucket-oauth2').Strategy;
const BITBUCKET_CLIENT_ID = "QVNRr44TW4gwmHpKWY";
const BITBUCKET_CLIENT_SECRET = "dyzPV7DsYZ9gyC8ZKRHG2HmZDNupd9Pr";
const cookieParser = require('cookie-parser');
const session = require('express-session');

router.use(express.json());
router.use(express.static('public'));
router.use(express.static('views'));
router.use(express.static('models'));
router.use(express.static('data'));
router.use(cookieParser());
router.use(session({
    secret: 'SEGReT$25_',
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false }
}));
router.use(passport.initialize());
router.use(passport.session());


//////
// const config = require("../config.js");
// const cloudinary = require('cloudinary');
// cloudinary.config({
//     cloud_name: config.cloudinary_cloud_name,
//     api_key: config.cloudinary_api_key,
//     api_secret: config.cloudinary_api_secret
// });

// passport.serializeUser(function (user, done) {
//     console.log(user.id);
//     done(null, user.login);
// });
// passport.deserializeUser(function (_id, done) {
//     user.getById(_id)
//         .then(user => {
//             done(user ? null : 'No user', user);
//         });
// });
passport.use(new BitbucketStrategy({ clientID: BITBUCKET_CLIENT_ID, clientSecret: BITBUCKET_CLIENT_SECRET, callbackURL: "http://localhost:3000/auth/bitbucket" },
    function (token, tokenSecret, profile, done) {
        let _profile = profile;
        user.insertOA(_profile)
            .then(() => { return user.getOrCreateByLogin(_profile); })
            .then(user => {
                if (user) {
                    done(null, user);
                } else {
                    done(null, false);
                }
            })
            .catch(err => { console.log(err); });

    }
));
router.get('/auth/bitbucket',
    passport.authenticate('bitbucket', { failureRedirect: 'auth/login' }),
    function (req, res) {
        res.redirect('/');
    });
module.exports = router;
