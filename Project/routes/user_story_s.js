const story = require('../models/story.js');
const mongoose = require('mongoose');

let express = require('express');
let router = express.Router();

router.use(express.json());
router.use(express.static('public'));
router.use(express.static('views'));
router.use(express.static('models'));
router.use(express.static('data'));

//////
const config = require("../config.js");
const cloudinaryStorage = require('multer-storage-cloudinary');
const multer = require('multer');
const cloudinary = require('cloudinary');
cloudinary.config({
   cloud_name: config.cloudinary_cloud_name,
   api_key: config.cloudinary_api_key,
   api_secret: config.cloudinary_api_secret
});
const storage = cloudinaryStorage({
   cloudinary: cloudinary,
   folder: 'folder-name',
   allowedFormats: ['jpg', 'png'],
   transformation: [{ width: 400, height: 400, crop: "limit" }]
});
const upload = multer({ storage: storage });
////////////////////////////////////////////////////////////////////////////////////

router.get('/user_stories', checkAuth, function (req, res) {
   const pageSize = 4;

   if (req.query.search) {
      let search = req.query.search.toLowerCase();
      story.findByRole(search, req.user.id)
         .then((filteredStories) => {
            let searchResult;

            for (let i = 0; i < filteredStories.length; i++) {
               if (!filteredStories[i].role.toLowerCase().includes(search)) {
                  filteredStories.splice(i, 1);
                  i--;
               }
            }
            searchResult = (filteredStories.length === 0) ? 'No results for ' + search: 'Displaying stories for ' + search + ".Results found:" +filteredStories.length ;

            let pagesTotal = Math.ceil(filteredStories.length / pageSize);
            let page = (typeof req.query.p === 'undefined' || req.query.p < 1 || req.query.p > pagesTotal) ? 1 : parseInt(req.query.p);
            let prev = (page !== 1) ? (page - 1) : 1;
            let next = (page !== pagesTotal) ? (page + 1) : pagesTotal;
            res.render('user_stories', {
               searchResult: searchResult,
               title: "User stories",
               user_stories: filteredStories.slice(page * pageSize - pageSize, page * pageSize),
               page: page,
               prevPage: "?p=" + prev,
               nextPage: "?p=" + next,
               id_: filteredStories.id,
               role_: filteredStories.role,
               pageCount: pagesTotal,
               filteredSearch: "&search=" + String(req.query.search),
               user: req.user
            });
         })
         .catch(err => res.send(err));
   }
   else {

      story.getAll(req.user.id)
         .then(stories => {
            let pagesTotal = Math.ceil(stories.length / pageSize);
            let page = (typeof req.query.p === 'undefined' || req.query.p < 1 || req.query.p > pagesTotal) ? 1 : parseInt(req.query.p);
            let prev = (page !== 1) ? (page - 1) : 1;
            let next = (page !== pagesTotal) ? (page + 1) : pagesTotal;
            res.render('user_stories', {
               searchResult: "",
               title: "User stories",
               user_stories: stories.slice(page * pageSize - pageSize, page * pageSize),
               page: page,
               prevPage: "?p=" + prev,
               nextPage: "?p=" + next,
               id_: stories.id,
               role_: stories.role,
               pageCount: pagesTotal,
               user: req.user
            });
         })
         .catch(err => res.send(err));
   }

});

router.route('/new_story')
   .get(function (req, res) { res.render('new_story', { title: " new_story", user: req.user }); })
   .post(upload.single('file'), function (req, res) {
      if (req.file) {
         let dat = Date.now();
         story.insert(new story(null, dat, req.body.role, req.body.iWantTo, req.body.points, req.file.url, req.user.id))
            .then(newStory => {
               res.redirect('user_stories/' + newStory.id);
            })
            .catch(res.status(500));
      } else {
         let dat = Date.now();
         story.insert(new story(null, dat, req.body.role, req.body.iWantTo, req.body.points, "https://res.cloudinary.com/drpv1113r/image/upload/v1573195885/onmtedklywezi7enlxmn.png", req.user.id))
            .then(newStory => {
               res.redirect('user_stories/' + newStory.id);
            })
            .catch(res.status(500));
      }
   });

router.route('/user_stories/:id')
   .get(function (req, res) {
      if (!mongoose.Types.ObjectId.isValid(req.params.id)) return res.status(404).render('not_found');

      story.getById(req.params.id)
         .then(story =>
            res.render('user_story', {
               title: story.id,
               id: story.id,
               role: story.role,
               iWantTo: story.iWantTo,
               points: story.points,
               image: story.image,
               user: req.user
            })
         )
         .catch(err => res.status(500).send(err.toString()));
   })
   .post(upload.none(), function (req, res) {

      if (req.body.updateUser) {
         console.log("REDIRECTING");
         res.redirect('/update_userstory/' + req.params.id);
      }
      if (req.body.delete) {
         story.getById(req.params.id)
            .then(storyToDelete => {
               return story.deleteById(req.params.id);
            })
            .then(() => res.redirect('/user_stories'))
            .catch(err => res.status(500).send(err.toString()));
      }

   });

router.route('/update_userstory/:id')
   .get(checkAuth, function (req, res) {
      if (!mongoose.Types.ObjectId.isValid(req.params.id)) return res.status(404).render('not_found');
      console.log("GOTCHA");
      story.getById(req.params.id)
         .then(storen => {
            res.render('update_userstory', {
               title: "Update story",
               user_story: storen,
               user: req.user
            });
         })
         .catch(err => res.status(500).send(err.toString()));
   })
   .post(checkAuth, upload.single('file'), function (req, res) {
      let newAva;

      story.getById(req.params.id)
         .then(storen => {
            newAva = storen.image;
            if (req.file) { newAva = req.file.url; }
            let dataToUpdate = {
               role: req.body.role,
               iWantTo: req.body.iWantTo,
               points: req.body.points,
               image: newAva,
               user: req.user

            };
            return story.update(req.params.id, dataToUpdate);
         })
         .then(() => {
            res.redirect('/user_stories/' + req.params.id);
         })
         .catch(err => res.status(500).send(err.toString()));
   });
////////////////////////////////////////////////////////////////////////////////////

function checkAuth(req, res, next) {
   if (!req.user) return res.sendStatus(401); // 'Not authorized'
   next();  // пропускати далі тільки аутентифікованих
}
module.exports = router;