// auth.js -

let express = require('express');
let router = express.Router();
const user = require('../models/user.js');
const config = require("../config.js");
const cloudinaryStorage = require('multer-storage-cloudinary');
const crypto = require('crypto');
const serverSalt = config.secretKey;

router.use(express.json());
router.use(express.static('public'));
router.use(express.static('views'));
router.use(express.static('models'));
router.use(express.static('data'));
//////
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const cookieParser = require('cookie-parser');
const session = require('express-session');
//////
////////////////passport
router.use(cookieParser());
router.use(session({
    secret: 'SEGReT$25_',
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false }
}));
router.use(passport.initialize());
router.use(passport.session());
passport.serializeUser(function (user, done) {
    console.log("STEP SERIALIZE");
    done(null, user._id);
});
passport.deserializeUser(function (_id, done) {
    console.log("STEP deserialize");

    user.getById(_id)
        .then(user => {
            done(user ? null : 'No user', user);
        });
});
function sha512(password, salt) {
    const hash = crypto.createHmac('sha512', salt);
    hash.update(password);
    const value = hash.digest('hex');
    return {
        salt: salt,
        passwordHash: value
    };
};
passport.use(new LocalStrategy(
    function (username, password, done) {
        console.log("STEP STRATEGY");
        let hash = sha512(password, serverSalt).passwordHash;
        user.getUserByLoginAndPasshash(username, hash)
            .then(user => {
                console.log("LS getOrCreateByLogin" +user);

                if (user) {
                    done(null, user);
                } else {
                    done(null, false);
                }
            });
    }
));
const multer = require('multer');
const cloudinary = require('cloudinary');
cloudinary.config({
    cloud_name: config.cloudinary_cloud_name,
    api_key: config.cloudinary_api_key,
    api_secret: config.cloudinary_api_secret
});
const storage = cloudinaryStorage({
    cloudinary: cloudinary,
    folder: 'folder-name',
    allowedFormats: ['jpg', 'png'],
    transformation: [{ width: 400, height: 400, crop: "limit" }]
});

const upload = multer({ storage: storage });

router.get('/register', upload.none(), function (req, res) {
    res.render('register.mst', { error: req.query.error });
});
router.post('/register', upload.none(), function (req, res) {
        let pass = req.body.pass;
            let hash = sha512(pass, serverSalt).passwordHash;
            let gname = req.body.groupName;
            user.insert(req.body.login, gname, hash)
                .then(writeRes => {
                    if (writeRes.upserted) {
                        res.redirect('login');
                    }
                    else {
                        res.redirect('register');
                    }
                })
                .catch(res.status(500));
    
});

router.get('/login', function (req, res) {
    let errMes = req.query.error;
    res.render('login.mst',
        { error: errMes });
});
router.post('/login', upload.none(), passport.authenticate('local', { failureRedirect: '/auth/login?error=Wrong%20login%20or%20password' }), function (req, res) {
    res.redirect('/');
});

router.get('/logout', checkAuth, upload.none(), function (req, res) {
    req.logout();
    res.redirect('/');
});

function checkAuth(req, res, next) {
    if (!req.user) return res.sendStatus(401); // 'Not authorized'
    next();  // пропускати далі тільки аутентифікованих
}
/////////////////////////


module.exports = router;