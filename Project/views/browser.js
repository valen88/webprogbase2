
///////////////////////////////////modal///////////////////////////  
let modal = document.getElementById("delete");
// Get the button that opens the modal
let delete_btn = document.getElementById("myBtn");
let ooops_button = document.getElementById("ooops_button");
let confirm_button = document.getElementById("confirm_button");
// Get the <span> element that closes the modal
let span = document.getElementsByClassName("close")[0];
// When the user clicks on the button, open the modal
delete_btn.onclick = function() {
  modal.style.display = "block";
};
confirm_button.onclick = function() {
  modal.style.display = "block";
};
// When the user clicks, close the modal
span.onclick = function() {
  modal.style.display = "none";
};
ooops_button.onclick = function(){
  modal.style.display = "none";
};
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
};
///////////////////////////////////modal///////////////////////////  
