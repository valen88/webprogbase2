module.exports = {};

const mongoose = require('mongoose');

let Schema = mongoose.Schema;
const UserSchema = new Schema({
    login: { type: String, required: true },
    groupName: { type: String },
    role: { type: Boolean },
    fullname: { type: String, default: this.login },
    registeredAt: { type: Date, default: Date.now },
    avaUrl: { type: String, default: "https://res.cloudinary.com/drpv1113r/image/upload/v1573203865/logo_x8lutp.png" },
    about: { type: String, default: "I am a user" },
    passHash: { type: String }
});

const UserModel = mongoose.model('User', UserSchema);
class User {

    constructor(id, login, role, fullname, registeredAt, avaUrl, about, passHash, groupName) {
        this.id = id; // number
        this.login = login;  // string
        this.role = role;// number 0 - user 1 -admin
        this.fullname = fullname;// string
        this.registeredAt = registeredAt;// string
        this.avaUrl = avaUrl;// string
        this.about = about;
        this.passHash = passHash;
        this.groupName = groupName;
    }
    static getAll() {
        return UserModel.find();
    }

    static getById(id) {
        return UserModel.findById(id);
    }
    static checkUniqueLogin(_login) {
        return UserModel.find({ login: { $exists: true, $eq: _login } });
    }
    static insert(_login, _groupName, _passHash) {
        return UserModel.updateOne(
            { login: _login },
            {
                $setOnInsert: {
                    role: false,
                    fullname: _login,
                    groupName: _groupName,
                    passHash: _passHash
                }
            },
            { upsert: true }
        );
    }
    static insertOA(_user) {
        let raw = JSON.parse(_user._raw);
        return UserModel.updateOne({ login: _user.username }, { avaUrl: raw.links.avatar.href, $setOnInsert: { groupName: "default", fullname: _user.displayName } }, { upsert: true });;
    }
    static update(id, newData) {
        return UserModel.updateOne({ _id: id }, newData, { upsert: true });
    }
    static getUserByLoginAndPasshash(_login, _passhash) {
        return UserModel.findOne({ login: { $eq: _login }, passHash: { $eq: _passhash } });
    }
    static changeRole(user) {
        let newRole = !user.role;
        return UserModel.updateOne({ _id: user.id }, { role: newRole });
    }
    static deleteById(id) {
        return UserModel.deleteOne({ _id: id });
    }
    static checkLoginUniqueness(newLogin) {
        return UserModel.findOne({ login: newLogin });
    }
    static getOrCreateByLogin(_user) {
        return UserModel.findOne({ login: { $eq: _user.username } });

    }
}

module.exports = User;
