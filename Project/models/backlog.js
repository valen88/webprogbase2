module.exports = {};

const mongoose = require('mongoose');
let Schema = mongoose.Schema;
const BacklogSchema = new Schema({
    name: { type: String ,required : true},
    avaUrl: { type: String ,default:"https://res.cloudinary.com/drpv1113r/image/upload/v1573195885/onmtedklywezi7enlxmn.png"},
    groupName : {type:String, default:"admin" ,required : true}
});

const BacklogModel = mongoose.model('Backlog', BacklogSchema);


class Backlog {
    constructor(name, avaUrl,groupName) {
        this.name = name;// number
        this.avaUrl = avaUrl; // string
        this.groupName =groupName;
    }
    static getById(id) {
        return BacklogModel.findById(id);
    }
    static getAll(groupName) {
        return BacklogModel.find({groupName:groupName});
    }
    static insert(backlog) {
        return new BacklogModel(backlog).save();
    }
    static update(id,_name) {
        return BacklogModel.updateOne({ _id: id }, {name : _name});
    }
    static imageCheck(image) {
        return BacklogModel.find({ avaUrl: image });
    }
    static deleteById(id) {
        return BacklogModel.deleteOne({ _id: id });
    }
    static findByName(_name,groupName){
        return BacklogModel.find({name:_name , groupName:groupName});
    }
    


}
module.exports = Backlog;