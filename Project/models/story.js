module.exports = {};

const mongoose = require('mongoose');
let Schema = mongoose.Schema;
const StorySchema = new Schema({
    role: { type: String },
    iWantTo: { type: String },
    points: { type: Number },
    registeredAt: { type: Date },
    image: { type: String ,default:"https://res.cloudinary.com/drpv1113r/image/upload/v1573195885/onmtedklywezi7enlxmn.png"},
    backlogID : {type : String, default: ""},
    userID:{type : String}
});

const StoryModel = mongoose.model('Story', StorySchema);


class Story {
    constructor(id, registeredAt, role, iWantTo, points, image,userID) {
        this.id = id;// number
        this.role = role; // string
        this.iWantTo = iWantTo; // string
        this.points = points;// number
        this.registeredAt = registeredAt; // string
        this.image = image;
        this.userID = userID;
    }

    static getById(id) {
        return StoryModel.findById(id);
    }



    static getAll(userID) {
        return StoryModel.find({userID:userID});
    }

    static insert(story) {
        return new StoryModel(story).save();
    }
    static insertBL(whom,how){
        return StoryModel.updateOne({_id: whom},how);
    }
    static deleteById(id) {
        return StoryModel.deleteOne({ _id: id });
    }
    static getAllStoriesByBacklogId(id) {
        return StoryModel.find({backlogID : id});
    }
    static update(id, newData) {
        return StoryModel.updateOne({ _id: id }, newData,{ upsert: true });
    }
    static getAllFree() {
        return StoryModel.find({backlogID : ""});
     }
     static findByRole(_role,userID){
        return StoryModel.find({role : _role ,userID:userID});
     }


}
module.exports = Story;