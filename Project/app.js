const mustache = require("mustache-express");
const path = require('path');
const express = require('express');
const app = express();
const config = require("./config.js");
const BITBUCKET_CLIENT_ID = "QVNRr44TW4gwmHpKWY";
const BITBUCKET_CLIENT_SECRET = "dyzPV7DsYZ9gyC8ZKRHG2HmZDNupd9Pr";
app.set('json spaces', 2);
const passport = require('passport');
const cookieParser = require('cookie-parser');
const session = require('express-session');


///////////////////////////
const mongoose = require('mongoose');
 const dbUrl = config.dbUrl;

const connectOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
};
mongoose.connect(dbUrl, connectOptions)
.then(() => console.log('Mongo database connected'))
.catch(() => console.log('ERROR: Mongo database not connected'));
////////////////////////////////
app.use(cookieParser());
app.use(session({
   secret: 'SEGReT$25_',
   resave: false,
   saveUninitialized: true,
   cookie: { secure: false }
}));
app.use(passport.initialize());
app.use(passport.session());

const port = config.ServerPort;
app.listen(port, () => console.log(` app listening on port ${port}!`));

app.use(express.static('public'));
app.use(express.static('views'));
app.use(express.static('models'));
app.use(express.static('data'));
app.use(express.static('routes'));


const viewsDir = path.join(__dirname, 'views');
app.engine("mst", mustache(path.join(viewsDir, "partials")));
app.set('views', viewsDir);
app.set('view engine', 'mst');

let auth = require('./routes/auth.js');
app.use('/auth', auth);
////////////
let developer = require('./routes/developer.js');
app.use('/developer/v1', developer);
////////////
let api = require('./routes/api.js');
app.use('/api/v1', api);
////////////
let main_s = require('./routes/main_s.js');
app.use('', main_s);
////////////
let user_s = require('./routes/user_s.js');
app.use('', user_s);
////////////
let user_story_s = require('./routes/user_story_s.js');
app.use('', user_story_s);
////////////
let bb = require('./routes/bbauth.js');
app.use('', bb);
////////////
let backlog_s = require('./routes/backlog_s.js');
app.use('', backlog_s);
////////////

app.use(function(req, res){
   res.status(404);
   res.render('not_found');
 });
// function deleteImageCloudinary(image) {
//    return new Promise((resolve, reject) => {
//        cloudinary.v2.api.delete_resources(image,
//            (error, result) => {
//                if (error) reject(error);
//                else resolve(result);
//            });
//    });
// }